import React, { useState } from "react";
import "./App.css";
import CubeContainer from "./Components/CubeContainer/CubeContainer";

const App = () => {
  const [cube, setCube] = useState([]);
  const [count, setCount] = useState(0);

  if (cube.length === 0) {
    const arr = [];

    const random = Math.floor(Math.random() * 35 + 1);

    for (let i = 0; i < 36; i++) {
      arr.push({ class: "Cube-black", text: ""});
    };

    arr[random].text = "0";

    setCube(arr);
  };

  const colorCube = (index) => {
    const cubeCopy = [...cube];
    let countCopy = count;

    if (cubeCopy[index].class !== "Cube-red") {
      if (cubeCopy[index].class !== "Winner") {
        countCopy++;
        cubeCopy[index].class = "Cube-red";
      };
    };

    setCube(cubeCopy);
    setCount(countCopy);

    if (cubeCopy[index].text === "0") {
      setCube([{ text: "Вы Выиграли!", class: "Winner" }]);
    };
  };

  const newGame = () => {
    let cubeCopy = [...cube];
    cubeCopy = [];
    setCube(cubeCopy);

    let countCopy = count;
    countCopy = 0;
    setCount(countCopy);
  };

  return (
    <div className="App">
      <CubeContainer
        cube={cube}
        count={count}
        colorCube={colorCube}
        onClickHandler={newGame}
      />
    </div>
  );
};

export default App;
