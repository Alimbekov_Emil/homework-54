import React from "react";
import "./Count.css";

const Count = (props) => {
  return <h1 className="Counter">Счет : {props.count}</h1>;
};

export default Count;
