import React from "react";
import "./ButtonReset.css";

const ButtonReset = (props) => {
  return (
    <button className="Btn" onClick={props.onClick}>
      Reset
    </button>
  );
};

export default ButtonReset;
