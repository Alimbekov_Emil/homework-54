import React from "react";
import "./Cube.css";

const Cube = (props) => {
  return (
    <div className={props.className} onClick={props.color}>
      <b>{props.ring}</b>
    </div>
  );
};

export default Cube;
