import React from "react";
import "./CubeContainer.css";
import ButtonReset from "../ButtonReset/ButtonReset";
import Cube from "../Cube/Cube";
import Count from "../Count/Count";

const CubeContainer = (props) => {
  const cubeArr = props.cube.map((cube, index) => {
    return (
      <Cube
        key={index}
        className={cube.class}
        color={() => props.colorCube(index)}
        ring={cube.text}
      />
    );
  });

  return (
    <div>
      <div className="CubeContainer">{cubeArr}</div>
      <div className="Block-count">
        <Count count={props.count} />
        <ButtonReset onClick={props.onClickHandler} />
      </div>
    </div>
  );
};

export default CubeContainer;
